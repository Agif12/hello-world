const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

// function to calculate of prisma
function prisma(luasAlas, tinggi) {
  return luasAlas * tinggi;
}

function inputLuasAlas() {
  rl.question(`Input Luas Alas:`, (luasAlas) => {
    if (!isNaN(luasAlas)) {
      inputTinggi(luasAlas);
    } else {
      console.log(`luas Alas Must be a number!\n`);
      inputLuasAlas();
    }
  });
}

function inputTinggi(luasAlas, tinggi) {
  rl.question(`Input Tinggi:`, (tinggi) => {
    if (!isNaN(tinggi)) {
      console.log(`\nHasil Volume Prisma: ${prisma(luasAlas, tinggi)}`);
      rl.close();
    } else {
      console.log(`Tinggi must be a number!`);
      inputTinggi(luasAlas, tinggi);
    }
  });
}

console.log(`Volume Of Prisma`);
console.log(`=========`);
inputLuasAlas();
